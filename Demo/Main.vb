Imports System.Threading

Imports isr.Visuals.GnuPlot

Friend Class Demo
    <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
    Public Shared Sub Main(ByVal args() As String)
        Dim unused As New Random()
        Dim x As Double()
        Dim y As Double()
        Dim z As Double()
        Dim Z2 As Double(,)

        isr.Visuals.GnuPlot.Gnu.GnuPlot.StartProcess(TimeSpan.FromMilliseconds(3000))

        isr.Visuals.GnuPlot.Gnu.GnuPlot.Plot("sin(x) + 2")

        Console.WriteLine("Enter key 0")
        Console.ReadKey()

        Gnu.GnuPlot.Plot("sin(x) + 2", "lc rgb ""magenta"" lw 5")
        ' Thread.Sleep(2000);
        Console.WriteLine("Enter key 1")
        Console.ReadKey()

        Gnu.GnuPlot.HoldOn()
        Gnu.GnuPlot.Plot("cos(x) + x")
        Gnu.GnuPlot.Plot("cos(2*x)", "with points pt 3")

        ' Thread.Sleep(2000);
        Console.WriteLine("Enter key 2")
        Console.ReadKey()

        Gnu.GnuPlot.HoldOff()
        Gnu.GnuPlot.Plot("atan(x)")

        ' Thread.Sleep(2000);
        Console.WriteLine("Enter key 3")
        Console.ReadKey()


        x = New Double() {-10, -8.5, -2, 1, 6, 9, 10, 14, 15, 19}
        y = New Double() {-4, 6.5, -2, 3, -8, -5, 11, 4, -5, 10}
        Gnu.GnuPlot.Plot(x, y)

        ' Thread.Sleep(2000);
        Console.WriteLine("Enter key 4")
        Console.ReadKey()


        Dim tempfolder As String = System.IO.Path.GetTempPath()
        Gnu.GnuPlot.SaveData(x, y, tempfolder & "plot1.data")
        Gnu.GnuPlot.Plot(tempfolder & "plot1.data", "with linespoints pt " & CInt(Fix(Gnu.PointStyle.SolidDiamond)))

        ' Thread.Sleep(2000);
        Console.WriteLine("Enter key 5")
        Console.ReadKey()


        Gnu.GnuPlot.HoldOn()
        Gnu.GnuPlot.Set("xrange [-25:40]")
        Gnu.GnuPlot.Set("yrange [-15:15]")
        Dim r As Random = New Random()

        For i As Integer = 0 To 13
            Dim Xr As Double() = New Double(9) {}
            Dim Yr As Double() = New Double(9) {}
            Dim rx As Double = r.Next(-20, 20)
            Dim ry As Double = r.Next(-10, 10)
            For di As Integer = 0 To 9
                Xr(di) = rx + r.Next(-5, 5)
                Yr(di) = ry + r.Next(-3, 3)
            Next di

            Gnu.GnuPlot.Plot(Xr, Yr, "title 'point style " & i & "' pt " & i)
            Thread.Sleep(200)
        Next i
        Gnu.GnuPlot.HoldOff()

        ' Thread.Sleep(2000);
        Console.WriteLine("Enter key 6")
        Console.ReadKey()

        ' splot demos
        z = New Double(31 * 31 - 1) {}
        For xx As Integer = 0 To 30
            For yy As Integer = 0 To 30
                z(31 * xx + yy) = (xx - 15) * (xx - 15) + (yy - 15) * (yy - 15)
            Next yy
        Next xx
        Gnu.GnuPlot.Set("pm3d")
        Gnu.GnuPlot.Set("autoscale")
        Gnu.GnuPlot.Set("contour base")
        Gnu.GnuPlot.SPlot(31, z)

        ' Thread.Sleep(2000);
        Console.WriteLine("Enter key 7")
        Console.ReadKey()

        Gnu.GnuPlot.HoldOn()
        Gnu.GnuPlot.Set("view map")
        Gnu.GnuPlot.Unset("surface")
        Gnu.GnuPlot.Set("cntrparam levels 10")
        Gnu.GnuPlot.Set("palette gray")
        Gnu.GnuPlot.SPlot(31, z)

        ' Thread.Sleep(2000);
        Console.WriteLine("Enter key 8")
        Console.ReadKey()

        y = New Double() {-4, 6.5, -2, 3, -8, -5, 11, 4, -5, 10}
        Gnu.GnuPlot.Plot(y)

        Console.WriteLine("Enter key 9")
        Console.ReadKey()

        Gnu.GnuPlot.SPlot("1 / (.05*x*x + .05*y*y + 1)")
        Console.WriteLine("Enter key 14")
        Console.ReadKey()

        Gnu.GnuPlot.Set("isosamples 30")
        Gnu.GnuPlot.SPlot("1 / (.05*x*x + .05*y*y + 1)")

        ' Thread.Sleep(2000);
        Console.WriteLine("Enter key 15")
        Console.ReadKey()

        Gnu.GnuPlot.Set("isosamples 30", "hidden3d")
        Gnu.GnuPlot.SPlot("1 / (.05*x*x + .05*y*y + 1)")

        ' Thread.Sleep(2000);
        Console.WriteLine("Enter key 16")
        Console.ReadKey()

        Gnu.GnuPlot.SPlot("splotexampledata.txt")

        ' TO_DO: Figure out how to clear here.
        Gnu.GnuPlot.EndProcess()
        Gnu.GnuPlot.StartProcess(TimeSpan.FromMilliseconds(3000))

        ' Thread.Sleep(2000);
        Console.WriteLine("Enter key 17")
        Console.ReadKey()

        ' TO_DO: Figure out how to fix this
        ' z = New Double() {-4, -2.5, 1, 3, -3, -2, 3, 4, -1, 2, 6, 8}
        ' Gnu.GnuPlot.Set("pm3d")
        ' Gnu.GnuPlot.SPlot(4, z)

        ' Thread.Sleep(2000);
        Console.WriteLine("Enter key18 - Test skipped ")
        Console.ReadKey()

        Z2 = New Double(,) {{-4, -2.5, 1, 3}, {-3, -2, 3, 4}, {-1, 2, 6, 8}}
        Gnu.GnuPlot.Set("pm3d", "palette gray")
        Gnu.GnuPlot.SPlot(Z2, "with points pointtype 6")

        ' Thread.Sleep(2000);
        Console.WriteLine("Enter key 19")
        Console.ReadKey()

        x = New Double(99) {}
        y = New Double(99) {}
        z = New Double(99) {}
        r = New Random()
        For i As Integer = 0 To 99
            x(i) = r.Next(30) - 15
            y(i) = r.Next(50) - 25
            z(i) = r.Next(20) - 10
        Next i

        Gnu.GnuPlot.Set("xrange[-30:30]", "yrange[-30:30]", "zrange[-30:30]")
        Gnu.GnuPlot.SPlot(x, y, z, "with points pointtype 8 lc rgb ""blue""")


        ' Thread.Sleep(2000);
        Console.WriteLine("Enter key 20")
        Console.ReadKey()

        x = New Double(19) {}
        y = New Double(19) {}
        z = New Double(19) {}
        r = New Random()
        For i As Integer = 0 To 19
            x(i) = r.Next(30) - 15
            y(i) = r.Next(50) - 25
            z(i) = r.Next(40) - 20
        Next i

        Gnu.GnuPlot.Set("dgrid3d 40,40,2")
        Gnu.GnuPlot.Set("xrange[-30:30]", "yrange[-30:30]", "zrange[-30:30]")
        Gnu.GnuPlot.SPlot(x, y, z, "with pm3d")

        ' Thread.Sleep(2000);
        Console.WriteLine("Enter key 21")
        Console.ReadKey()

        Gnu.GnuPlot.Unset("key")
        Gnu.GnuPlot.Set("cntrparam levels 20", "isosamples 50", "xr[-5:5]", "yr[-6:6]")
        Gnu.GnuPlot.Contour("sin(x) * cos(y)+x", "lc rgb 'blue'")

        Console.WriteLine("Enter key 22")
        Console.ReadKey()

        ' TO_DO: Figure out how to clear here.
        Gnu.GnuPlot.EndProcess()
        Gnu.GnuPlot.StartProcess(TimeSpan.FromMilliseconds(3000))

        Z2 = New Double(,) {{0, 0, 0, 1, 2, 2, 1, 0, 0, 0},
                            {0, 0, 2, 3, 3, 3, 3, 2, 0, 0},
                            {0, 2, 3, 4, 4, 4, 4, 3, 2, 0},
                            {2, 3, 4, 5, 5, 5, 5, 4, 3, 2},
                            {3, 4, 5, 6, 7, 7, 6, 5, 4, 3},
                            {3, 4, 5, 6, 7, 7, 6, 5, 4, 3},
                            {2, 3, 4, 5, 5, 5, 5, 4, 3, 2},
                            {0, 2, 3, 4, 4, 4, 4, 3, 2, 0},
                            {0, 0, 2, 3, 3, 3, 3, 2, 0, 0},
                            {0, 0, 0, 1, 2, 2, 1, 0, 0, 0}}
        Gnu.GnuPlot.HeatMap(Z2)

        Console.WriteLine("End of demo")
        Console.ReadKey()

        Gnu.GnuPlot.EndProcess()

    End Sub
End Class
