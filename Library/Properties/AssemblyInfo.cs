﻿using System;
using System.Reflection;

[assembly: AssemblyTitle(isr.Visuals.GnuPlot.My.MyLibrary.AssemblyTitle)]
[assembly: AssemblyDescription(isr.Visuals.GnuPlot.My.MyLibrary.AssemblyDescription)]
[assembly: AssemblyProduct(isr.Visuals.GnuPlot.My.MyLibrary.AssemblyProduct)]
[assembly: CLSCompliant(true)]
[assembly: System.Runtime.InteropServices.ComVisible(false)]
