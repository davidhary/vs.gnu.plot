
namespace isr.Visuals.GnuPlot.My
{

    /// <summary> Provides assembly information for the class library. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
    public sealed partial class MyLibrary
    {

        /// <summary>
        /// Constructor that prevents a default instance of this class from being created.
        /// </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        private MyLibrary() : base()
        {
        }

        /// <summary> Gets the identifier of the trace source. </summary>
        public const int TraceEventId = (int) isr.Core.ProjectTraceEventId.GnuPLot;

        /// <summary> The assembly title. </summary>
        public const string AssemblyTitle = "Core Services Library";

        /// <summary> Information describing the assembly. </summary>
        public const string AssemblyDescription = "Core Services Library";

        /// <summary> The assembly product. </summary>
        public const string AssemblyProduct = "Visuals.GnuPlot";
    }
}
