using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;

namespace isr.Visuals.GnuPlot.Gnu
{

    /// <summary> Wrapper around the Gnu Plot process. </summary>
        /// <remarks>
        /// (c) 2011 Awoke Knowing. All rights reserved.<para>
        /// Licensed under The MIT License.</para><para>
        /// David, 2019-06-17. Source: https://github.com/AwokeKnowing/GnuplotCSharp.
        /// </para>
        /// </remarks>
    public sealed class GnuPlot
    {

        #region " CONSTRUCTION "

        /// <summary>
        /// Constructor that prevents a default instance of this class from being created.
        /// </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        private GnuPlot() : base()
        {
            ProgramFolder = _DefaultProgramFolder;
        }

        #endregion

        #region " PROCESS "

        /// <summary> The active process. </summary>
        /// <value> The active process. </value>
        public static Process ActiveProcess { get; private set; }

        /// <summary> Start a new Gnu Plot process. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="timeout"> The timeout. </param>
        /// <returns> The Process. </returns>
        public static Process StartProcess(TimeSpan timeout)
        {
            ProgramFolder = string.IsNullOrWhiteSpace(ProgramFolder) ? _DefaultProgramFolder : ProgramFolder;
            ActiveProcess = new Process();
            ActiveProcess.StartInfo.FileName = $"{ProgramFolder}{_DefaultProgramFile}";
            ActiveProcess.StartInfo.UseShellExecute = false;
            ActiveProcess.StartInfo.RedirectStandardInput = true;
            if (ActiveProcess.Start())
            {
                _ActiveStreamWriter = ActiveProcess.StandardInput;
                _PlotBuffer = new List<StoredPlot>();
                _SPlotBuffer = new List<StoredPlot>();
                Set("terminal wxt size 350,262");
                Hold = false;
                _ = ActiveProcess.WaitForInputIdle( ( int ) timeout.TotalMilliseconds );
            }

            return ActiveProcess;
        }

        /// <summary> Ends the process. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        public static void EndProcess()
        {
            Hold = false;
            ActiveProcess?.Dispose();
        }

        /// <summary> The default program title. </summary>
        private const string _DefaultProgramTitle = "gnuplot";
        /// <summary> The default program file. </summary>
        private const string _DefaultProgramFile = _DefaultProgramTitle + ".exe";
        /// <summary> The default program folder. </summary>
        private const string _DefaultProgramFolder = @"C:\Program Files\" + _DefaultProgramTitle + @"\bin\";

        /// <summary> Pathname of the program folder. </summary>
        private static string _ProgramFolder;

        /// <summary> Pathname of the program folder. </summary>
        /// <value> The pathname of the program folder. </value>
        public static string ProgramFolder
        {
            get => _ProgramFolder;

            set {
                if ( string.IsNullOrWhiteSpace( value ) )
                {
                    value = _DefaultProgramFolder;
                }

                if ( !value.EndsWith( @"\", StringComparison.OrdinalIgnoreCase ) )
                {
                    value += @"\";
                }

                _ProgramFolder = value;
            }
        }

        /// <summary> Buffer for plot data. </summary>
        private static List<StoredPlot> _PlotBuffer;

        /// <summary> Buffer for plot data. </summary>
        private static List<StoredPlot> _SPlotBuffer;

        /// <summary> True to replot with splot. </summary>
        private static bool _ReplotWithSplot;

        /// <summary> True to hold. </summary>
        /// <value> The hold. </value>
        public static bool Hold { get; private set; }

        /// <summary> Hold on. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        public static void HoldOn()
        {
            Hold = true;
            _PlotBuffer.Clear();
            _SPlotBuffer.Clear();
        }

        /// <summary> Hold off. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        public static void HoldOff()
        {
            Hold = false;
            _PlotBuffer.Clear();
            _SPlotBuffer.Clear();
        }

        /// <summary> Closes this object. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        public static void Close()
        {
            _ = ActiveProcess.CloseMainWindow();
        }

        #endregion

        #region " STREAM "

        /// <summary> The active stream writer. </summary>
        private static StreamWriter _ActiveStreamWriter;

        /// <summary> Writes a line. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="commands"> The commands to write. </param>
        public static void WriteLine(string commands)
        {
            _ActiveStreamWriter.WriteLine(commands);
            _ActiveStreamWriter.Flush();
        }

        /// <summary> Writes. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="commands"> The commands to write. </param>
        public static void Write(string commands)
        {
            _ActiveStreamWriter.Write(commands);
            _ActiveStreamWriter.Flush();
        }

        /// <summary>   Sets the given options. </summary>
        /// <remarks>   David, 2020-09-17. </remarks>
        /// <param name="options">  Options for controlling the operation. </param>
        public static void Set(params string[] options)
        {
            if (options?.Any() == true)
            {
                for (int i = 0, loopTo = options.Length - 1; i <= loopTo; i++)
                {
                    _ActiveStreamWriter.WriteLine("set " + options[i]);
                }
            }
        }

        /// <summary> Unsets the given options. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="options"> Options for controlling the operation. </param>
        public static void Unset(params string[] options)
        {
            if (options?.Any() == true)
            {
                for (int i = 0, loopTo = options.Length - 1; i <= loopTo; i++)
                {
                    _ActiveStreamWriter.WriteLine("unset " + options[i]);
                }
            }
        }

        /// <summary> Saves a data. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="values">   The values. </param>
        /// <param name="fileName"> Filename of the file. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public static bool SaveData(double[] values, string fileName)
        {
            using (var dataStream = new StreamWriter(fileName, false))
            {
                WriteData(values, dataStream);
            }

            return true;
        }

        /// <summary> Saves a data. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="abscissa"> The abscissa. </param>
        /// <param name="ordinate"> The ordinate. </param>
        /// <param name="fileName"> Filename of the file. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public static bool SaveData(double[] abscissa, double[] ordinate, string fileName)
        {
            if (abscissa is null)
            {
                throw new ArgumentNullException(nameof(abscissa));
            }

            if (ordinate is null)
            {
                throw new ArgumentNullException(nameof(ordinate));
            }

            using (var dataStream = new StreamWriter(fileName, false))
            {
                WriteData(abscissa, ordinate, dataStream);
            }

            return true;
        }

        /// <summary> Saves a data. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="abscissa"> The abscissa. </param>
        /// <param name="ordinate"> The ordinate. </param>
        /// <param name="depth">    The depth. </param>
        /// <param name="fileName"> Filename of the file. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public static bool SaveData(double[] abscissa, double[] ordinate, double[] depth, string fileName)
        {
            if (abscissa is null)
            {
                throw new ArgumentNullException(nameof(abscissa));
            }

            if (ordinate is null)
            {
                throw new ArgumentNullException(nameof(ordinate));
            }

            using (var dataStream = new StreamWriter(fileName, false))
            {
                WriteData(abscissa, ordinate, depth, dataStream);
            }

            return true;
        }

        /// <summary> Saves a data. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="ordinateSize"> Size of the ordinate. </param>
        /// <param name="depth">        The depth. </param>
        /// <param name="fileName">     Filename of the file. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public static bool SaveData(int ordinateSize, double[] depth, string fileName)
        {
            if (depth is null)
            {
                throw new ArgumentNullException(nameof(depth));
            }

            using (var dataStream = new StreamWriter(fileName, false))
            {
                WriteData(ordinateSize, depth, dataStream);
            }

            return true;
        }

        /// <summary> Saves a data. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="plane">    The plane. </param>
        /// <param name="fileName"> Filename of the file. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        [CLSCompliant(false)]
        public static bool SaveData(double[,] plane, string fileName)
        {
            using (var dataStream = new StreamWriter(fileName, false))
            {
                WriteData(plane, dataStream);
            }

            return true;
        }

        #endregion

        #region " PLOT "

        /// <summary> Replots this object. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        public static void Replot()
        {
            if (_ReplotWithSplot)
            {
                SPlot(_SPlotBuffer);
            }
            else
            {
                Plot(_PlotBuffer);
            }
        }

        /// <summary> Plots the given stored plots. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="fileNameOrFunction"> The file name or function. </param>
        /// <param name="options">            (Optional) Options for controlling the operation. </param>
        public static void Plot(string fileNameOrFunction, string options = "")
        {
            if (!Hold)
            {
                _PlotBuffer.Clear();
            }

            _PlotBuffer.Add(new StoredPlot(fileNameOrFunction, options));
            Plot(_PlotBuffer);
        }

        /// <summary> Plots the given stored plots. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="ordinate"> The ordinate. </param>
        /// <param name="options">  (Optional) Options for controlling the operation. </param>
        public static void Plot(double[] ordinate, string options = "")
        {
            if (ordinate is null)
            {
                throw new ArgumentNullException(nameof(ordinate));
            }

            if (!Hold)
            {
                _PlotBuffer.Clear();
            }

            _PlotBuffer.Add(new StoredPlot(ordinate, options));
            Plot(_PlotBuffer);
        }

        /// <summary> Plots the given stored plots. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="abscissa"> The abscissa. </param>
        /// <param name="ordinate"> The ordinate. </param>
        /// <param name="options">  (Optional) Options for controlling the operation. </param>
        public static void Plot(double[] abscissa, double[] ordinate, string options = "")
        {
            if (abscissa is null)
            {
                throw new ArgumentNullException(nameof(abscissa));
            }

            if (ordinate is null)
            {
                throw new ArgumentNullException(nameof(ordinate));
            }

            if (!Hold)
            {
                _PlotBuffer.Clear();
            }

            _PlotBuffer.Add(new StoredPlot(abscissa, ordinate, options));
            Plot(_PlotBuffer);
        }

        /// <summary> Contours. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="fileNameOrFunction"> The file name or function. </param>
        /// <param name="options">            (Optional) Options for controlling the operation. </param>
        /// <param name="labelContours">      (Optional) True to label contours. </param>
        public static void Contour(string fileNameOrFunction, string options = "", bool labelContours = true)
        {
            if (!Hold)
            {
                _PlotBuffer.Clear();
            }

            var p = new StoredPlot(fileNameOrFunction, options, PlotType.ContourFileOrFunction) { LabelContours = labelContours };
            _PlotBuffer.Add(p);
            Plot(_PlotBuffer);
        }

        /// <summary> Contours. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="ordinateSize">  Size of the ordinate. </param>
        /// <param name="depth">         The depth. </param>
        /// <param name="options">       (Optional) Options for controlling the operation. </param>
        /// <param name="labelContours"> (Optional) True to label contours. </param>
        public static void Contour(int ordinateSize, double[] depth, string options = "", bool labelContours = true)
        {
            if (depth is null)
            {
                throw new ArgumentNullException(nameof(depth));
            }

            if (!Hold)
            {
                _PlotBuffer.Clear();
            }

            var p = new StoredPlot(ordinateSize, depth, options, PlotType.ContourZ) { LabelContours = labelContours };
            _PlotBuffer.Add(p);
            Plot(_PlotBuffer);
        }

        /// <summary> Contours. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="abscissa">      The abscissa. </param>
        /// <param name="ordinate">      The ordinate. </param>
        /// <param name="depth">         The depth. </param>
        /// <param name="options">       (Optional) Options for controlling the operation. </param>
        /// <param name="labelContours"> (Optional) True to label contours. </param>
        public static void Contour(double[] abscissa, double[] ordinate, double[] depth, string options = "", bool labelContours = true)
        {
            if (abscissa is null)
            {
                throw new ArgumentNullException(nameof(abscissa));
            }

            if (ordinate is null)
            {
                throw new ArgumentNullException(nameof(ordinate));
            }

            if (depth is null)
            {
                throw new ArgumentNullException(nameof(depth));
            }

            if (!Hold)
            {
                _PlotBuffer.Clear();
            }

            var p = new StoredPlot(abscissa, ordinate, depth, options, PlotType.ContourXYZ) { LabelContours = labelContours };
            _PlotBuffer.Add(p);
            Plot(_PlotBuffer);
        }

        /// <summary> Contours. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="plane">         The plane. </param>
        /// <param name="options">       (Optional) Options for controlling the operation. </param>
        /// <param name="labelContours"> (Optional) True to label contours. </param>
        public static void Contour(double[,] plane, string options = "", bool labelContours = true)
        {
            if (!Hold)
            {
                _PlotBuffer.Clear();
            }

            var p = new StoredPlot(plane, options, PlotType.ContourZZ) { LabelContours = labelContours };
            _PlotBuffer.Add(p);
            Plot(_PlotBuffer);
        }

        /// <summary> Heat map. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="fileNameOrFunction"> The file name or function. </param>
        /// <param name="options">            (Optional) Options for controlling the operation. </param>
        public static void HeatMap(string fileNameOrFunction, string options = "")
        {
            if (!Hold)
            {
                _PlotBuffer.Clear();
            }

            _PlotBuffer.Add(new StoredPlot(fileNameOrFunction, options, PlotType.ColorMapFileOrFunction));
            Plot(_PlotBuffer);
        }

        /// <summary> Heat map. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="ordinateSize"> Size of the ordinate. </param>
        /// <param name="intensity">    The intensity. </param>
        /// <param name="options">      (Optional) Options for controlling the operation. </param>
        public static void HeatMap(int ordinateSize, double[] intensity, string options = "")
        {
            if (intensity is null)
            {
                throw new ArgumentNullException(nameof(intensity));
            }

            if (!Hold)
            {
                _PlotBuffer.Clear();
            }

            _PlotBuffer.Add(new StoredPlot(ordinateSize, intensity, options, PlotType.ColorMapZ));
            Plot(_PlotBuffer);
        }

        /// <summary> Heat map. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="abscissa">  The abscissa. </param>
        /// <param name="ordinate">  The ordinate. </param>
        /// <param name="intensity"> The intensity. </param>
        /// <param name="options">   (Optional) Options for controlling the operation. </param>
        public static void HeatMap(double[] abscissa, double[] ordinate, double[] intensity, string options = "")
        {
            if (abscissa is null)
            {
                throw new ArgumentNullException(nameof(abscissa));
            }

            if (ordinate is null)
            {
                throw new ArgumentNullException(nameof(ordinate));
            }

            if (intensity is null)
            {
                throw new ArgumentNullException(nameof(intensity));
            }

            if (!Hold)
            {
                _PlotBuffer.Clear();
            }

            _PlotBuffer.Add(new StoredPlot(abscissa, ordinate, intensity, options, PlotType.ColorMapXYZ));
            Plot(_PlotBuffer);
        }

        /// <summary> Heat map. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="intensityGrid"> The intensity grid. </param>
        /// <param name="options">       (Optional) Options for controlling the operation. </param>
        public static void HeatMap(double[,] intensityGrid, string options = "")
        {
            if (!Hold)
            {
                _SPlotBuffer.Clear();
            }

            _PlotBuffer.Add(new StoredPlot(intensityGrid, options, PlotType.ColorMapZZ));
            Plot(_PlotBuffer);
        }

        /// <summary> Plots the given stored plots. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="storedPlots"> The stored plots. </param>
        public static void Plot(IList<StoredPlot> storedPlots)
        {
            if (storedPlots is null)
            {
                throw new ArgumentNullException(nameof(storedPlots));
            }

            _ReplotWithSplot = false;
            string plotActionName = "plot ";
            string plotstring = string.Empty;
            string contfile;
            string defcntopts;
            RemoveContourLabels();
            for (int i = 0, loopTo = storedPlots.Count - 1; i <= loopTo; i++)
            {
                var p = storedPlots[i];
                defcntopts = p.Options.Length > 0 && (p.Options.Contains(" w") || p.Options[0] == 'w') ? " " : " with lines ";
                switch (p.PlotType)
                {
                    case PlotType.PlotFileOrFunction:
                        {
                            if (p.FileName is object)
                            {
                                plotstring += plotActionName + PlotPath(p.FileName) + " " + p.Options;
                            }
                            else
                            {
                                plotstring += plotActionName + p.FunctionName + " " + p.Options;
                            }

                            break;
                        }

                    case PlotType.PlotXY:
                    case PlotType.PlotY:
                        {
                            plotstring += plotActionName + "\"-\" " + p.Options;
                            break;
                        }

                    case PlotType.ContourFileOrFunction:
                        {
                            contfile = Path.GetTempPath() + "_cntrtempdata" + i + ".dat";
                            MakeContourFile(p.FileName is object ? PlotPath(p.FileName) : p.FunctionName, contfile);
                            if (p.LabelContours)
                            {
                                SetContourLabels(contfile);
                            }

                            plotstring += plotActionName + PlotPath(contfile) + defcntopts + p.Options;
                            break;
                        }

                    case PlotType.ContourXYZ:
                        {
                            contfile = Path.GetTempPath() + "_cntrtempdata" + i + ".dat";
                            MakeContourFile(p.Abscissa, p.Ordinate, p.Depth, contfile);
                            if (p.LabelContours)
                            {
                                SetContourLabels(contfile);
                            }

                            plotstring += plotActionName + PlotPath(contfile) + defcntopts + p.Options;
                            break;
                        }

                    case PlotType.ContourZZ:
                        {
                            contfile = Path.GetTempPath() + "_cntrtempdata" + i + ".dat";
                            MakeContourFile(p.Plane, contfile);
                            if (p.LabelContours)
                            {
                                SetContourLabels(contfile);
                            }

                            plotstring += plotActionName + PlotPath(contfile) + defcntopts + p.Options;
                            break;
                        }

                    case PlotType.ContourZ:
                        {
                            contfile = Path.GetTempPath() + "_cntrtempdata" + i + ".dat";
                            MakeContourFile(p.OrdinateSize, p.Depth, contfile);
                            if (p.LabelContours)
                            {
                                SetContourLabels(contfile);
                            }

                            plotstring += plotActionName + PlotPath(contfile) + defcntopts + p.Options;
                            break;
                        }

                    case PlotType.ColorMapFileOrFunction:
                        {
                            if (p.FileName is object)
                            {
                                plotstring += plotActionName + PlotPath(p.FileName) + " with image " + p.Options;
                            }
                            else
                            {
                                plotstring += plotActionName + p.FunctionName + " with image " + p.Options;
                            }

                            break;
                        }

                    case PlotType.ColorMapXYZ:
                    case PlotType.ColorMapZ:
                        {
                            plotstring += plotActionName + "\"-\" " + " with image " + p.Options;
                            break;
                        }

                    case PlotType.ColorMapZZ:
                        {
                            plotstring += plotActionName + "\"-\" " + "matrix with image " + p.Options;
                            break;
                        }
                }

                if (i == 0)
                {
                    plotActionName = ", ";
                }
            }

            _ActiveStreamWriter.WriteLine(plotstring);
            for (int i = 0, loopTo1 = storedPlots.Count - 1; i <= loopTo1; i++)
            {
                var p = storedPlots[i];
                switch (p.PlotType)
                {
                    case PlotType.PlotXY:
                        {
                            WriteData(p.Abscissa, p.Ordinate, _ActiveStreamWriter, false);
                            _ActiveStreamWriter.WriteLine("e");
                            break;
                        }

                    case PlotType.PlotY:
                        {
                            WriteData(p.Ordinate, _ActiveStreamWriter, false);
                            _ActiveStreamWriter.WriteLine("e");
                            break;
                        }

                    case PlotType.ColorMapXYZ:
                        {
                            WriteData(p.Abscissa, p.Ordinate, p.Depth, _ActiveStreamWriter, false);
                            _ActiveStreamWriter.WriteLine("e");
                            break;
                        }

                    case PlotType.ColorMapZ:
                        {
                            WriteData(p.OrdinateSize, p.Depth, _ActiveStreamWriter, false);
                            _ActiveStreamWriter.WriteLine("e");
                            break;
                        }

                    case PlotType.ColorMapZZ:
                        {
                            WriteData(p.Plane, _ActiveStreamWriter, false);
                            _ActiveStreamWriter.WriteLine("e");
                            break;
                        }
                }
            }

            _ActiveStreamWriter.Flush();
        }

        #endregion

        #region " S PLOT "

        /// <summary> Plots the given stored plots. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="fileNameOrFunction"> The file name or function. </param>
        /// <param name="options">            (Optional) Options for controlling the operation. </param>
        public static void SPlot(string fileNameOrFunction, string options = "")
        {
            if (!Hold)
            {
                _SPlotBuffer.Clear();
            }

            _SPlotBuffer.Add(new StoredPlot(fileNameOrFunction, options, PlotType.SplotFileOrFunction));
            SPlot(_SPlotBuffer);
        }

        /// <summary> Plots the given stored plots. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="ordinateSize"> Size of the ordinate. </param>
        /// <param name="depth">        The depth. </param>
        /// <param name="options">      (Optional) Options for controlling the operation. </param>
        public static void SPlot(int ordinateSize, double[] depth, string options = "")
        {
            if (depth is null)
            {
                throw new ArgumentNullException(nameof(depth));
            }

            if (!Hold)
            {
                _SPlotBuffer.Clear();
            }

            _SPlotBuffer.Add(new StoredPlot(ordinateSize, depth, options));
            SPlot(_SPlotBuffer);
        }

        /// <summary> Plots the given stored plots. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="abscissa"> The abscissa. </param>
        /// <param name="ordinate"> The ordinate. </param>
        /// <param name="depth">    The depth. </param>
        /// <param name="options">  (Optional) Options for controlling the operation. </param>
        public static void SPlot(double[] abscissa, double[] ordinate, double[] depth, string options = "")
        {
            if (abscissa is null)
            {
                throw new ArgumentNullException(nameof(abscissa));
            }

            if (ordinate is null)
            {
                throw new ArgumentNullException(nameof(ordinate));
            }

            if (depth is null)
            {
                throw new ArgumentNullException(nameof(depth));
            }

            if (!Hold)
            {
                _SPlotBuffer.Clear();
            }

            _SPlotBuffer.Add(new StoredPlot(abscissa, ordinate, depth, options));
            SPlot(_SPlotBuffer);
        }

        /// <summary> Plots the given stored plots. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="plane">   The plane. </param>
        /// <param name="options"> (Optional) Options for controlling the operation. </param>
        public static void SPlot(double[,] plane, string options = "")
        {
            if (!Hold)
            {
                _SPlotBuffer.Clear();
            }

            _SPlotBuffer.Add(new StoredPlot(plane, options));
            SPlot(_SPlotBuffer);
        }

        /// <summary> Plots the given stored plots. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="storedPlots"> The stored plots. </param>
        public static void SPlot(IList<StoredPlot> storedPlots)
        {
            if (storedPlots is null)
            {
                throw new ArgumentNullException(nameof(storedPlots));
            }

            _ReplotWithSplot = true;
            string splotActionName = "splot ";
            string plotstring = string.Empty;
            RemoveContourLabels();
            for (int i = 0, loopTo = storedPlots.Count - 1; i <= loopTo; i++)
            {
                var p = storedPlots[i];
                string defopts = p.Options.Length > 0 && (p.Options.Contains(" w") || p.Options[0] == 'w') ? " " : " with lines ";
                switch (p.PlotType)
                {
                    case PlotType.SplotFileOrFunction:
                        {
                            if (p.FileName is object)
                            {
                                plotstring += splotActionName + PlotPath(p.FileName) + defopts + p.Options;
                            }
                            else
                            {
                                plotstring += splotActionName + p.FunctionName + defopts + p.Options;
                            }

                            break;
                        }

                    case PlotType.SplotXYZ:
                    case PlotType.SplotZ:
                        {
                            plotstring += splotActionName + "\"-\" " + defopts + p.Options;
                            break;
                        }

                    case PlotType.SplotZZ:
                        {
                            plotstring += splotActionName + "\"-\" matrix " + defopts + p.Options;
                            break;
                        }
                }

                if (i == 0)
                {
                    splotActionName = ", ";
                }
            }

            _ActiveStreamWriter.WriteLine(plotstring);
            for (int i = 0, loopTo1 = storedPlots.Count - 1; i <= loopTo1; i++)
            {
                var p = storedPlots[i];
                switch (p.PlotType)
                {
                    case PlotType.SplotXYZ:
                        {
                            WriteData(p.Abscissa, p.Ordinate, p.Depth, _ActiveStreamWriter, false);
                            _ActiveStreamWriter.WriteLine("e");
                            break;
                        }

                    case PlotType.SplotZZ:
                        {
                            WriteData(p.Plane, _ActiveStreamWriter, false);
                            _ActiveStreamWriter.WriteLine("e");
                            break;
                        }
                    // GnupStWr.WriteLine("e")
                    case PlotType.SplotZ:
                        {
                            WriteData(p.OrdinateSize, p.Depth, _ActiveStreamWriter, false);
                            _ActiveStreamWriter.WriteLine("e");
                            break;
                        }
                }
            }

            _ActiveStreamWriter.Flush();
        }

        #endregion

        #region " WRITE DATA "

        /// <summary> Writes a data. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="values"> The values. </param>
        /// <param name="stream"> The stream. </param>
        /// <param name="flush">  (Optional) True to flush. </param>
        public static void WriteData(double[] values, StreamWriter stream, bool flush = true)
        {
            if (values is null)
            {
                throw new ArgumentNullException(nameof(values));
            }

            if (stream is null)
            {
                throw new ArgumentNullException(nameof(stream));
            }

            for (int i = 0, loopTo = values.Length - 1; i <= loopTo; i++)
            {
                stream.WriteLine(values[i].ToString());
            }

            if (flush)
            {
                stream.Flush();
            }
        }

        /// <summary> Writes a data. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="abscissa"> The abscissa. </param>
        /// <param name="ordinate"> The ordinate. </param>
        /// <param name="stream">   The stream. </param>
        /// <param name="flush">    (Optional) True to flush. </param>
        public static void WriteData(double[] abscissa, double[] ordinate, StreamWriter stream, bool flush = true)
        {
            if (abscissa is null)
            {
                throw new ArgumentNullException(nameof(abscissa));
            }

            if (ordinate is null)
            {
                throw new ArgumentNullException(nameof(ordinate));
            }

            if (stream is null)
            {
                throw new ArgumentNullException(nameof(stream));
            }

            for (int i = 0, loopTo = Math.Min(abscissa.Length, ordinate.Length) - 1; i <= loopTo; i++)
            {
                stream.WriteLine($"{abscissa[i]} {ordinate[i]}");
            }

            if (flush)
            {
                stream.Flush();
            }
        }

        /// <summary> Writes a data. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="ordinateSize"> Size of the ordinate. </param>
        /// <param name="depth">        The depth. </param>
        /// <param name="stream">       The stream. </param>
        /// <param name="flush">        (Optional) True to flush. </param>
        public static void WriteData(int ordinateSize, double[] depth, StreamWriter stream, bool flush = true)
        {
            if (depth is null)
            {
                throw new ArgumentNullException(nameof(depth));
            }

            if (stream is null)
            {
                throw new ArgumentNullException(nameof(stream));
            }

            for (int i = 0, loopTo = depth.Length - 1; i <= loopTo; i++)
            {
                if (i > 0 && i % ordinateSize == 0)
                {
                    stream.WriteLine();
                }

                stream.WriteLine(depth[i].ToString());
            }

            if (flush)
            {
                stream.Flush();
            }
        }

        /// <summary> Writes a data. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="plane">  The plane. </param>
        /// <param name="stream"> The stream. </param>
        /// <param name="flush">  (Optional) True to flush. </param>
        [CLSCompliant(false)]
        public static void WriteData(double[,] plane, StreamWriter stream, bool flush = true)
        {
            if (plane is null)
            {
                throw new ArgumentNullException(nameof(plane));
            }

            if (stream is null)
            {
                throw new ArgumentNullException(nameof(stream));
            }

            int m = plane.GetLength(0);
            int n = plane.GetLength(1);
            string line;
            for (int i = 0, loopTo = m - 1; i <= loopTo; i++)
            {
                line = string.Empty;
                for (int j = 0, loopTo1 = n - 1; j <= loopTo1; j++)
                {
                    line = $"{line}{plane[i, j]} ";
                }

                stream.WriteLine(line.TrimEnd());
            }

            if (flush)
            {
                stream.Flush();
            }
        }

        /// <summary> Writes a data. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="abscissa"> The abscissa. </param>
        /// <param name="ordinate"> The ordinate. </param>
        /// <param name="depth">    The depth. </param>
        /// <param name="stream">   The stream. </param>
        /// <param name="flush">    (Optional) True to flush. </param>
        public static void WriteData(double[] abscissa, double[] ordinate, double[] depth, StreamWriter stream, bool flush = true)
        {
            if (abscissa is null)
            {
                throw new ArgumentNullException(nameof(abscissa));
            }

            if (ordinate is null)
            {
                throw new ArgumentNullException(nameof(ordinate));
            }

            if (depth is null)
            {
                throw new ArgumentNullException(nameof(depth));
            }

            if (stream is null)
            {
                throw new ArgumentNullException(nameof(stream));
            }

            for (int i = 0, loopTo = Math.Min(Math.Min(abscissa.Length, ordinate.Length), depth.Length) - 1; i <= loopTo; i++)
            {
                if (i > 0 && abscissa[i] != abscissa[i - 1])
                {
                    stream.WriteLine("");
                }

                stream.WriteLine($"{abscissa[i]} {ordinate[i]} {depth[i]}");
            }

            if (flush)
            {
                stream.Flush();
            }
        }

        #endregion

        #region " CONTOUR FILE "

        /// <summary> Plot path. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="path"> Full pathname of the file. </param>
        /// <returns> A String. </returns>
        private static string PlotPath(string path)
        {
            return "\"" + path.Replace(@"\", @"\\") + "\"";
        }

        /// <summary> Saves a set state. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="fileName"> (Optional) Filename of the file. </param>
        public static void SaveSetState(string fileName = null)
        {
            if (fileName is null)
            {
                fileName = Path.GetTempPath() + "setstate.tmp";
            }

            _ActiveStreamWriter.WriteLine($"save set {PlotPath(fileName)}");
            _ActiveStreamWriter.Flush();
            _ = WaitForFile( fileName );
        }

        /// <summary> Loads set state. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="fileName"> (Optional) Filename of the file. </param>
        public static void LoadSetState(string fileName = null)
        {
            if (fileName is null)
            {
                fileName = $"{Path.GetTempPath()}setstate.tmp";
            }

            _ActiveStreamWriter.WriteLine($"load {PlotPath(fileName)}");
            _ActiveStreamWriter.Flush();
        }

        // these makecontourFile functions should probably be merged into one function and use a StoredPlot parameter

        /// <summary> Makes contour file. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="fileNameOrFunction"> The file name or function. </param>
        /// <param name="outputFile">         The output file. </param>
        private static void MakeContourFile(string fileNameOrFunction, string outputFile) // if it's a file, fileOrFunction needs quotes and escaped backslashes
        {
            SaveSetState();
            Set($"table {PlotPath(outputFile)}");
            Set("contour base");
            Unset("surface");
            _ActiveStreamWriter.WriteLine($"splot {fileNameOrFunction}");
            Unset("table");
            _ActiveStreamWriter.Flush();
            LoadSetState();
            _ = WaitForFile( outputFile );
        }

        /// <summary> Makes contour file. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="abscissa">   The abscissa. </param>
        /// <param name="ordinate">   The ordinate. </param>
        /// <param name="depth">      The depth. </param>
        /// <param name="outputFile"> The output file. </param>
        private static void MakeContourFile(double[] abscissa, double[] ordinate, double[] depth, string outputFile)
        {
            SaveSetState();
            Set("table " + PlotPath(outputFile));
            Set("contour base");
            Unset("surface");
            _ActiveStreamWriter.WriteLine("splot \"-\"");
            WriteData(abscissa, ordinate, depth, _ActiveStreamWriter);
            _ActiveStreamWriter.WriteLine("e");
            Unset("table");
            _ActiveStreamWriter.Flush();
            LoadSetState();
            _ = WaitForFile( outputFile );
        }

        /// <summary> Makes contour file. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="plane">      The plane. </param>
        /// <param name="outputFile"> The output file. </param>
        private static void MakeContourFile(double[,] plane, string outputFile)
        {
            SaveSetState();
            Set("table " + PlotPath(outputFile));
            Set("contour base");
            Unset("surface");
            _ActiveStreamWriter.WriteLine("splot \"-\" matrix");
            WriteData(plane, _ActiveStreamWriter);
            _ActiveStreamWriter.WriteLine("e");
            _ActiveStreamWriter.WriteLine("e");
            Unset("table");
            _ActiveStreamWriter.Flush();
            LoadSetState();
            _ = WaitForFile( outputFile );
        }

        /// <summary> Makes contour file. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="ordinateSize"> Size of the ordinate. </param>
        /// <param name="depth">        The depth. </param>
        /// <param name="outputFile">   The output file. </param>
        private static void MakeContourFile(int ordinateSize, double[] depth, string outputFile)
        {
            SaveSetState();
            Set("table " + PlotPath(outputFile));
            Set("contour base");
            Unset("surface");
            _ActiveStreamWriter.WriteLine("splot \"-\"");
            WriteData(ordinateSize, depth, _ActiveStreamWriter);
            _ActiveStreamWriter.WriteLine("e");
            Unset("table");
            _ActiveStreamWriter.Flush();
            LoadSetState();
            _ = WaitForFile( outputFile );
        }

        /// <summary> Number of contour labels. </summary>
        private static int _ContourLabelCount = 50000;

        /// <summary> Sets contour labels. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="contourFile"> The contour file. </param>
        private static void SetContourLabels(string contourFile)
        {
            var file = new StreamReader(contourFile);
            string line;
            line = file.ReadLine();
            while (line is object)
            {
                if (line.Contains("label:"))
                {
                    var c = file.ReadLine().Trim().Replace("   ", " ").Replace("  ", " ").Split(' ');
                    _ContourLabelCount += 1;
                    _ActiveStreamWriter.WriteLine("set object " + _ContourLabelCount + " rectangle center " + c[0] + "," + c[1] + " size char " + (c[2].ToString().Length + 1) + ",char 1 fs transparent solid .7 noborder fc rgb \"white\"  front");
                    _ActiveStreamWriter.WriteLine("set label " + _ContourLabelCount + " \"" + c[2] + "\" at " + c[0] + "," + c[1] + " front center");
                }

                line = file.ReadLine();
            }

            file.Close();
        }

        /// <summary> Removes the contour labels. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        private static void RemoveContourLabels()
        {
            while (_ContourLabelCount > 50000)
            {
                _ActiveStreamWriter.WriteLine("unset object " + _ContourLabelCount + ";unset label " + _ContourLabelCount);
                _ContourLabelCount -= 1;
            }
        }

        /// <summary> Wait for file. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="filename"> Filename of the file. </param>
        /// <param name="timeout">  (Optional) The timeout. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification = "Exception is published")]
        private static bool WaitForFile(string filename, int timeout = 10000)
        {
            Thread.Sleep(20);
            int attempts = timeout / 100;
            StreamReader file = null;
            while (file is null)
            {
                try
                {
                    file = new StreamReader(filename);
                }
                catch
                {
                    bool tempVar = attempts > 0;
                    attempts -= 1;
                    if (tempVar)
                    {
                        Thread.Sleep(100);
                    }
                    else
                    {
                        return false;
                    }
                }
            }

            file.Close();
            return true;
        }

        #endregion

    }

        /// <summary> Values that represent point styles. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
    public enum PointStyle
    {
        /// <summary> An enum constant representing the dot option. </summary>
        Dot = 0,
        /// <summary> An enum constant representing the plus option. </summary>
        Plus = 1,
        /// <summary> An enum constant representing the mark option. </summary>
        XMark = 2,
        /// <summary> An enum constant representing the star option. </summary>
        Star = 3,
        /// <summary> An enum constant representing the dot square option. </summary>
        DotSquare = 4,
        /// <summary> An enum constant representing the solid square option. </summary>
        SolidSquare = 5,
        /// <summary> An enum constant representing the dot circle option. </summary>
        DotCircle = 6,
        /// <summary> An enum constant representing the solid circle option. </summary>
        SolidCircle = 7,
        /// <summary> An enum constant representing the dot triangle up option. </summary>
        DotTriangleUp = 8,
        /// <summary> An enum constant representing the solid triangle up option. </summary>
        SolidTriangleUp = 9,
        /// <summary> An enum constant representing the dot triangle down option. </summary>
        DotTriangleDown = 10,
        /// <summary> An enum constant representing the solid triangle down option. </summary>
        SolidTriangleDown = 11,
        /// <summary> An enum constant representing the dot diamond option. </summary>
        DotDiamond = 12,
        /// <summary> An enum constant representing the solid diamond option. </summary>
        SolidDiamond = 13
    }

        /// <summary> Values that represent plot types. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
    public enum PlotType
    {
        /// <summary> An enum constant representing the plot file or function option. </summary>
        PlotFileOrFunction,
        /// <summary> An enum constant representing the plot Y coordinate option. </summary>
        PlotY,
        /// <summary> An enum constant representing the plot xy option. </summary>
        PlotXY,
        /// <summary> An enum constant representing the contour file or function option. </summary>
        ContourFileOrFunction,
        /// <summary> An enum constant representing the contour xyz option. </summary>
        ContourXYZ,
        /// <summary> An enum constant representing the contour zz option. </summary>
        ContourZZ,
        /// <summary> An enum constant representing the contour Z coordinate option. </summary>
        ContourZ,
        /// <summary> An enum constant representing the color map file or function option. </summary>
        ColorMapFileOrFunction,
        /// <summary> An enum constant representing the color map xyz option. </summary>
        ColorMapXYZ,
        /// <summary> An enum constant representing the color map zz option. </summary>
        ColorMapZZ,
        /// <summary> An enum constant representing the color map Z coordinate option. </summary>
        ColorMapZ,
        /// <summary> An enum constant representing the splot file or function option. </summary>
        SplotFileOrFunction,
        /// <summary> An enum constant representing the splot xyz option. </summary>
        SplotXYZ,
        /// <summary> An enum constant representing the splot zz option. </summary>
        SplotZZ,
        /// <summary> An enum constant representing the splot Z coordinate option. </summary>
        SplotZ
    }

        /// <summary> Wrapper around the Gnu Plot process. </summary>
        /// <remarks>
        /// (c) 2011 Awoke Knowing. All rights reserved.<para>
        /// Licensed under The MIT License.</para><para>
        /// David, 2019-06-17. Source: https://github.com/AwokeKnowing/GnuplotCSharp.
        /// </para>
        /// </remarks>
    public class StoredPlot
    {

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        public StoredPlot()
        {
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="fileNameOrFunction"> The file name or function. </param>
        /// <param name="options">            (Optional) Options for controlling the operation. </param>
        /// <param name="plotType">           (Optional) Type of the plot. </param>
        public StoredPlot(string fileNameOrFunction, string options = "", PlotType plotType = PlotType.PlotFileOrFunction) : base()
        {
            if (IsFile(fileNameOrFunction))
            {
                this.FileName = fileNameOrFunction;
            }
            else
            {
                this.FunctionName = fileNameOrFunction;
            }

            this.Options = options;
            this.PlotType = plotType;
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="ordinate"> The ordinate. </param>
        /// <param name="options">  (Optional) Options for controlling the operation. </param>
        public StoredPlot(double[] ordinate, string options = "") : base()
        {
            if (ordinate is null)
            {
                throw new ArgumentNullException(nameof(ordinate));
            }

            this.Ordinate = ordinate;
            this.Options = options;
            this.PlotType = PlotType.PlotY;
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="abscissa"> The abscissa. </param>
        /// <param name="ordinate"> The ordinate. </param>
        /// <param name="options">  (Optional) Options for controlling the operation. </param>
        public StoredPlot(double[] abscissa, double[] ordinate, string options = "") : base()
        {
            if (abscissa is null)
            {
                throw new ArgumentNullException(nameof(abscissa));
            }

            if (ordinate is null)
            {
                throw new ArgumentNullException(nameof(ordinate));
            }

            this.Abscissa = abscissa;
            this.Ordinate = ordinate;
            this.Options = options;
            this.PlotType = PlotType.PlotXY;
        }

        // 3D data

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="ordinateSize"> Size of the ordinate. </param>
        /// <param name="depth">        The depth. </param>
        /// <param name="options">      (Optional) Options for controlling the operation. </param>
        /// <param name="plotType">     (Optional) Type of the plot. </param>
        public StoredPlot(int ordinateSize, double[] depth, string options = "", PlotType plotType = PlotType.SplotZ) : base()
        {
            if (depth is null)
            {
                throw new ArgumentNullException(nameof(depth));
            }

            this.OrdinateSize = ordinateSize;
            this.Depth = depth;
            this.Options = options;
            this.PlotType = plotType;
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="abscissa"> The abscissa. </param>
        /// <param name="ordinate"> The ordinate. </param>
        /// <param name="depth">    The depth. </param>
        /// <param name="options">  (Optional) Options for controlling the operation. </param>
        /// <param name="plotType"> (Optional) Type of the plot. </param>
        public StoredPlot(double[] abscissa, double[] ordinate, double[] depth, string options = "", PlotType plotType = PlotType.SplotXYZ) : base()
        {
            if (abscissa is null)
            {
                throw new ArgumentNullException(nameof(abscissa));
            }

            if (ordinate is null)
            {
                throw new ArgumentNullException(nameof(ordinate));
            }

            if (depth is null)
            {
                throw new ArgumentNullException(nameof(depth));
            }

            if (abscissa.Length < 2)
            {
                this.OrdinateSize = 1;
            }
            else
            {
                for (int candidateSize = 1, loopTo = abscissa.Length - 1; candidateSize <= loopTo; candidateSize++)
                {
                    if (abscissa[candidateSize] != abscissa[candidateSize - 1])
                    {
                        this.OrdinateSize = candidateSize;
                        break;
                    }
                }
            }

            this.Depth = depth;
            this.Ordinate = ordinate;
            this.Abscissa = abscissa;
            this.Options = options;
            this.PlotType = plotType;
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="plane">    The plane. </param>
        /// <param name="options">  (Optional) Options for controlling the operation. </param>
        /// <param name="plotType"> (Optional) Type of the plot. </param>
        public StoredPlot(double[,] plane, string options = "", PlotType plotType = PlotType.SplotZZ) : base()
        {
            if (plane is null)
            {
                throw new ArgumentNullException(nameof(plane));
            }

            this.Plane = plane;
            this.Options = options;
            this.PlotType = plotType;
        }

        /// <summary> Query if 'functionOrFilename' is file. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="functionOrFilename"> Filename of the function or file. </param>
        /// <returns> <c>true</c> if file; otherwise <c>false</c> </returns>
        private static bool IsFile(string functionOrFilename)
        {
            int dot = functionOrFilename.LastIndexOf(".", StringComparison.OrdinalIgnoreCase);
            return dot >= 1 && (char.IsLetter(functionOrFilename[dot - 1]) || char.IsLetter(functionOrFilename[dot + 1]));
        }

        /// <summary> Gets or sets the filename of the file. </summary>
        /// <value> The name of the file. </value>
        public string FileName { get; private set; } = null;

        /// <summary> Gets or sets the name of the function. </summary>
        /// <value> The name of the function. </value>
        public string FunctionName { get; private set; } = null;

        /// <summary> Gets or sets the abscissa. </summary>
        /// <value> The abscissa. </value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Performance", "CA1819:Properties should not return arrays", Justification = "<Pending>")]
        public double[] Abscissa { get; private set; }

        /// <summary> Gets or sets the ordinate. </summary>
        /// <value> The ordinate. </value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Performance", "CA1819:Properties should not return arrays", Justification = "<Pending>")]
        public double[] Ordinate { get; private set; }

        /// <summary> Gets or sets the depth. </summary>
        /// <value> The depth. </value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Performance", "CA1819:Properties should not return arrays", Justification = "<Pending>")]
        public double[] Depth { get; private set; }

        /// <summary> Gets or sets the plane. </summary>
        /// <value> The plane. </value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Performance", "CA1819:Properties should not return arrays", Justification = "<Pending>")]
        public double[,] Plane { get; private set; }

        /// <summary> Gets or sets the size of the ordinate. </summary>
        /// <value> The size of the ordinate. </value>
        public int OrdinateSize { get; private set; }

        /// <summary> Gets or sets options for controlling the operation. </summary>
        /// <value> The options. </value>
        public string Options { get; private set; }

        /// <summary> Gets or sets the type of the plot. </summary>
        /// <value> The type of the plot. </value>
        public PlotType PlotType { get; private set; }

        /// <summary> Gets or sets the label contours. </summary>
        /// <value> The label contours. </value>
        public bool LabelContours { get; set; }
    }
}
