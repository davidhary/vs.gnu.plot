# vs.gnu.plot

wrapper for gnu plot


<a name="Source-Code"></a>
## Source Code
Clone the repository along with its requisite repositories to their respective relative path.

### Repositories
The repositories listed in [external repositories](ExternalReposCommits.csv) are required:
* [GnuPlot](https://www.bitbucket.org/davidhary/vs.visuals.gnuplot) - Gnu Plot Libraries

```
git clone git@bitbucket.org:davidhary/vs.gnu.plot.git
```

Clone the project along with its requisite projects to its respective relative path:
```
.\Libraries\VS\Visuals\GnuPlot
```

## Built, Tested and Facilitated By

* [Visual Studio](https://www.visualstudIO.com/) - Visual Studio 2015
* [Jarte](https://www.jarte.com/) - RTF Editor
* [Wix Installer](https://www.wixtoolset.org/) - Wix Toolset
* [Atomineer Code Documentation](https://www.atomineerutils.com/) - Code Documentation
* [EW Software](https://github.com/EWSoftware/VSSpellChecker/wiki/) - Spell Checker

## Authors

Forked from:
* [GnuplotCSharp](https://github.com/AwokeKnowing/GnuplotCSharp) 

## License

This repository is licensed under the [MIT License](https://www.bitbucket.org/davidhary/vs.gnu.plot/src/master/LICENSE.md)

## Acknowledgments

* [Its all a remix](www.everythingisaremix.info) -- we are but a spec on the shoulders of giants
* [Stack overflow](https://www.stackoveflow.com)

## Revision Changes

* [GnuplotCSharp](https://github.com/AwokeKnowing/GnuplotCSharp) 

