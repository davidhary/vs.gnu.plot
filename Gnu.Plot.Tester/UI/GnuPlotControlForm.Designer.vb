<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class GnuPlotControlForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me._GnuPlotParentControl = New Visuals.Gnu.Plot.GnuPlotParentControl()
        Me.SuspendLayout()
        '
        'GnuPlotParentControl1
        '
        Me._GnuPlotParentControl.Dock = System.Windows.Forms.DockStyle.Fill
        Me._GnuPlotParentControl.Location = New System.Drawing.Point(0, 0)
        Me._GnuPlotParentControl.Name = "GnuPlotParentControl1"
        Me._GnuPlotParentControl.Size = New System.Drawing.Size(800, 450)
        Me._GnuPlotParentControl.TabIndex = 0
        '
        'GnuPlotControlForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(800, 450)
        Me.Controls.Add(Me._GnuPlotParentControl)
        Me.Name = "GnuPlotControlForm"
        Me.Text = "Gnu Plot Control Form"
        Me.ResumeLayout(False)

    End Sub

    Private WithEvents _GnuPlotParentControl As GnuPlotParentControl
End Class
