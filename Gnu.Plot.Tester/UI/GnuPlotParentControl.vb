Imports System.Runtime.InteropServices

Public Class GnuPlotParentControl
    Inherits System.Windows.Forms.UserControl

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <param name="disposing"> <see langword="true" /> to release both managed and unmanaged
    '''                          resources; <see langword="false" /> to release only unmanaged
    '''                          resources. </param>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso Me.components IsNot Nothing Then
                Me.components.Dispose()
            End If
            Me._GnuPlotProcess?.Dispose()
            Me._GnuPlotProcess = Nothing
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " GNU PLOT "

    Private _GnuPlotProcess As Process

    ''' <summary> Raises the <see cref="E:System.Windows.Forms.UserControl.Load" /> event. </summary>
    ''' <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
	''' <Remarks> https://stackoverflow.com/questions/758494/how-can-i-run-another-application-within-a-panel-of-my-c-sharp-program </Remarks>
    Protected Overrides Sub OnLoad(e As EventArgs)
        MyBase.OnLoad(e)
        Me._GnuPlotProcess = isr.Visuals.GnuPlot.Gnu.GnuPlot.StartProcess(TimeSpan.FromMilliseconds(3000))
        isr.Visuals.GnuPlot.Gnu.GnuPlot.Plot("sin(x) + 2")
        ' this does nothing because this is not the handle to the Gnu Plot terminal.
        SafeNativeMethods.SetParent(Me._GnuPlotProcess.MainWindowHandle, Me.Handle)
        ' ! this crushes windows
        ' SafeNativeMethods.SetParent(SafeNativeMethods.GetForegroundWindow(), Me.Handle)
    End Sub

#End Region

End Class


Friend NotInheritable Class SafeNativeMethods
    Private Sub New()
        MyBase.New()
    End Sub
    <DllImport("user32.dll", CharSet:=CharSet.Auto, CallingConvention:=CallingConvention.Winapi)>
    Friend Shared Function SetParent(hWndChild As IntPtr, hWndNewParent As IntPtr) As IntPtr
    End Function

    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    <DllImport("user32.dll", EntryPoint:="GetForegroundWindow")>
    Friend Shared Function GetForegroundWindow() As IntPtr
    End Function

End Class


